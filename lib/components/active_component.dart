// ignore_for_file: use_key_in_widget_constructors, library_private_types_in_public_api, prefer_const_constructors

import 'package:car_shop/commons/data_provider.dart';
import 'package:car_shop/commons/order_widget.dart';
import 'package:car_shop/model/calling_model.dart';
import 'package:flutter/material.dart';

class ActiveComponent extends StatefulWidget {
  @override
  _ActiveComponentState createState() => _ActiveComponentState();
}

class _ActiveComponentState extends State<ActiveComponent> {
  List<CallingModel> activeData = activeDataList();

  @override
  void initState() {
    super.initState();
    init();
  }

  void init() async {
    //
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: activeData.length,
      padding: EdgeInsets.only(left: 16, bottom: 16, right: 16, top: 24),
      itemBuilder: (context, index) {
        CallingModel data = activeData[index];

        return OrderWidget(
            data: data, btnText1: "In Delivery", btnText2: "Track order");
      },
    );
  }
}
